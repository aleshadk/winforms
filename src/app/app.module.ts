import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeService } from './employee.service';
import { FormsModule }   from '@angular/forms';
import { CalendarModule, DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { EmployeeGridComponent } from './employee-grid/employee-grid.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { RouterModule, Routes } from '@angular/router';
import { TreeComponent } from './tree/tree.component';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { BrowseBoxComponent } from './browse-box/browse-box.component';
import { BrowseBoxDemoComponent } from './browse-box-demo/browse-box-demo.component';
import { WindowModule } from '@progress/kendo-angular-dialog';

const appRoutes: Routes = [
  { path: 'grid', component: EmployeeGridComponent },
  { path: 'form', component: EmployeeFormComponent },
  { path: 'tree', component: TreeComponent },
  { path: 'browse-box', component: BrowseBoxDemoComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeGridComponent,
    EmployeeFormComponent,
    TreeComponent,
    BrowseBoxComponent,
    BrowseBoxDemoComponent
  ],
  imports: [
    BrowserModule,
    GridModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    CalendarModule,
    DropDownsModule,
    TreeViewModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    InputsModule,
    DateInputsModule,
    WindowModule
  ],
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
