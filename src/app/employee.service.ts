import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Employee } from './_models/Employee';

interface IAnswer {
  results: Employee[];
}

@Injectable()
export class EmployeeService {
    private itemsPerPage = 100;

    constructor(private http: HttpClient) {}
      
    getData(page: number) {
      return this.http.get<IAnswer>(this.getUrl(page));
    }

    private getUrl(page: number): string {
      return `https://randomuser.me/api/?page=${page}&results=${this.itemsPerPage}&seed=abc`;
    }
}