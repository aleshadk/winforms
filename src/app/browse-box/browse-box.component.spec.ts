import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseBoxComponent } from './browse-box.component';

describe('BrowseBoxComponent', () => {
  let component: BrowseBoxComponent;
  let fixture: ComponentFixture<BrowseBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
