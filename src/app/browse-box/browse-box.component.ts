import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PageChangeEvent, GridDataResult, SelectableSettings } from '@progress/kendo-angular-grid';

@Component({
    selector: 'browse-box',
    templateUrl: './browse-box.component.html',
    styleUrls: ['./browse-box.component.css']
})
export class BrowseBoxComponent implements OnInit {    
    public get inputValue(): string { return this._inputValue };
    public set inputValue(value: string) {
        this._inputValue = value;
        this.filter();
    }

    public buttonDisabled = true;
    public skip = 0;
    public gridDataResult: GridDataResult;
    public selectableSettings: SelectableSettings = {
        mode: 'single',
        checkboxOnly: false
    };

    @Input() opened = false;
    @Input() header: string = '';
    @Input() gridHeaders: string[] = [];
    @Input() gridItems: {name: string, email: string}[] = [];
    @Input() pageSize: string = '10'; // #TODO
    @Output() onChange = new EventEmitter<string>();

    private _inputValue = '';
    private _selected: {name: string, email: string};

    /**
     * @description data form gridItems after filtering by _inputValue
     */
    private filteredGridItems: {name: string, email: string}[] = [];

    constructor() { }

    ngOnInit() {
        this.filter();
    }

    public submit(): void {
        alert('Selected ' + JSON.stringify(this._selected));
        this.opened = false;
    }

    public pageChange(event: PageChangeEvent): void {
        this.buttonDisabled = true;
        this.skip = event.skip;
        const data = this.filteredGridItems.slice(this.skip, this.skip + parseInt(this.pageSize));
        this.gridDataResult = {
            data,
            total: this.filteredGridItems.length
        }
    }

    public close(): void {
        this.opened = false;
    }

    public selectionChange($event) {
        this._selected = $event.selectedRows[0];
        this.buttonDisabled = false;
    }

    private filter(): void {
        const value = this._inputValue.toLowerCase();
        if (value.length === 0) {
            this.filteredGridItems = this.gridItems;
            this.pageChange({skip: 0, take: parseInt(this.pageSize)});
            return;
        }

        this.filteredGridItems = this.gridItems.filter(z => `${z.name}${z.email}`.toLowerCase().indexOf(value) > -1);
        this.pageChange({skip: 0, take: parseInt(this.pageSize)});
    }
}