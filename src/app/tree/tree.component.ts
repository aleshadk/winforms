import { Component, OnInit } from '@angular/core';
import { CheckableSettings } from '@progress/kendo-angular-treeview';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {
  public data = [
    {
      text: 'Music',
      items: [
        {
          text: 'Rock',
          items: [
            {
              text: 'Metal',
              items: [
                {
                  text: 'Heavy Metal'
                },
                {
                  text: 'Trash Metal'
                }
              ]
            },
            {
              text: 'Punk Rock'
            }
          ]
        },
        {
          text: 'Pop'
        },
        {
          text: 'Jazz',
          items: [
            {text: 'Fusion'},
            {text: 'Acid Jazz'},
          ]
        }
      ]
    }
  ];

  public get checkableSettings(): CheckableSettings {
    return {
        checkChildren: true,
        checkParents: true,
        enabled: true,
        mode: "multiple"
    };
}

  constructor() { }

  ngOnInit() {
  }

}
