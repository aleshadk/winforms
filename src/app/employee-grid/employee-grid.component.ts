import { Component, OnInit } from '@angular/core';
import { Employee } from '../_models/Employee';
import { EmployeeService } from '../employee.service';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { DataSourceRequestState, DataResult } from '@progress/kendo-data-query';

@Component({
  selector: 'employee-grid',
  templateUrl: './employee-grid.component.html',
  styleUrls: ['./employee-grid.component.css']
})
export class EmployeeGridComponent implements OnInit {
  public result: DataResult = {
    total: 0,
    data: []
  };

  public state: DataSourceRequestState = {
      skip: 0,
      take: 10
  };

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.result.total = 60; // Should get from server
    this.pageChange({skip: 0, take: 10});
  }

  public pageChange(event: PageChangeEvent): void {
    const page = (event.skip / event.take) + 1;
    this.employeeService.getData(page).subscribe(z => {
      this.result.data = z.results;
      this.state.skip = event.skip;
      this.state.take = event.take;
    });
  }
}
