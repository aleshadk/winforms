import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'employee-form',
    templateUrl: './employee-form.component.html',
    styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
    // Values to display
    public textboxValue: string;
    public maskValue: string;
    public radioValue: string;

    public autocompleteValue: string;
    public dropdownValue: string;
    public comboboxValue: string;

    public dateValue: Date;

    //Data
    public autocompleteData = [];

    // Static
    public mask = '0000-0000-0000-0000';
    public autocompleteSource = ['Moscow', 'Luxembourg', 'London', 'Boston', 'Paris', 'Berlin', 'Riga', 'Tallin', 'Madrid', 'Rome'];
    public dropdownData = [{ id: 1, value: 'a' }, { id: 2, value: 'b' }, { id: 3, value: 'c' }, { id: 4, value: 'd' }, { id: 5, value: 'e' }, { id: 6, value: 'f' }];

    ngOnInit() {
    }

    public autocompleteChange(value: any): void {
        this.autocompleteValue = value;
    }

    public autocompleteFilter(value: string) {
        value = value.toLowerCase();
        this.autocompleteData = this.autocompleteSource.filter(z => z.toLowerCase().indexOf(value) > -1);
    }

    public onRadioChange(value: string): void {
        this.radioValue = value;
    }
}
