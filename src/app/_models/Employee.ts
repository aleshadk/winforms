interface IEmployeeName {
    first: string;
    last: string;
}

export class Employee {
    public name: IEmployeeName;
    public email: string;
}