import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-browse-box-demo',
  templateUrl: './browse-box-demo.component.html',
  styleUrls: ['./browse-box-demo.component.css']
})
export class BrowseBoxDemoComponent implements OnInit {
  public show = false;
  public headers = [
    {field: 'name', title: 'User Name'},
    {field: 'email', title: 'Email'},
  ];
  public data = [
    {name: 'Peter', email: 'test1@example.com'},
    {name: 'John', email: 'test2@example.com'},
    {name: 'Polly', email: 'test3@example.com'},
    {name: 'Arthur', email: 'test4@example.com'},
    {name: 'Tommy', email: 'test5@example.com'},
    {name: 'Sara', email: 'test1@domain.com'},
    {name: 'Megan', email: 'test2@domain.com'},
    {name: 'Billy', email: 'test3@domain.com'},
    {name: 'Mark', email: 'test4@domain.com'},
    {name: 'Benny', email: 'test5@domain.com'},
    {name: 'Terry', email: 'test1@company.com'},
    {name: 'Chris', email: 'test2@company.com'},
    {name: 'Adam', email: 'test3@company.com'},
    {name: 'Routy', email: 'test4@company.com'},
    {name: 'Helga', email: 'test5@company.com'},
    {name: 'Imany', email: 'test1@web.com'},
    {name: 'Brandon', email: 'test2@web.com'},
    {name: 'Tony', email: 'test3@web.com'},
    {name: 'Samuel', email: 'test4@web.com'},
    {name: 'Maria', email: 'test5@web.com'},
    {name: 'Taylor', email: 'test1@test-domain.com'},
    {name: 'Robert', email: 'test2@test-domain.com'}
  ];


  constructor() { }

  ngOnInit() {
  }

  public onButtonClick(): void {
    this.show = true;
  }
}
