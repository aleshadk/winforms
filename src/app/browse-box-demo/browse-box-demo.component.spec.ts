import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseBoxDemoComponent } from './browse-box-demo.component';

describe('BrowseBoxDemoComponent', () => {
  let component: BrowseBoxDemoComponent;
  let fixture: ComponentFixture<BrowseBoxDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseBoxDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseBoxDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
